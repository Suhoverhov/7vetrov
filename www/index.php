<?php
define('__ROOT__', dirname(dirname(__FILE__)));
require_once (__ROOT__ . '/app/Autoload.php');
use app\Application;

$app = new Application();
$app->run();
