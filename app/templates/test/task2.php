<div class="page-header">
    <h1>Задание 2 </h1>
</div>
<p class="lead">Есть текстовые данные в следующем формате:
    url: DATA1
    get: DATA2
    post: DATA3
    Эти данные необходимо преобразовать в PHP массив, где ключ это get , url , post , а значение это DA ТА. Последовательность ключей – любая. Как только встретился
    хоть один ключ в тексте, то весь последующий текст необходимо преобразовывать в PHP массив:
    Решение задачи должно быть выстроено на основе использование регулярных выражений( perl -совместимых)
</p>
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Входные даные</h3>
    </div>
    <div class="panel-body">
        <p class="lead"><?php echo $input_data;?></p>
        <p class="lead">Текст хранится в файле - <pre><?php echo$filePath;?></pre></p>
    </div>
</div>
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Результат</h3>
    </div>
    <div class="panel-body">
        <ul>
            <?php foreach($data as $key=>$item):?>
                <li>
                    <code><?php echo $key?></code>
                    <ul>
                        <?php foreach($item as $text):?>
                            <li><code><?php echo $text?></code></li>
                        <?php endforeach ?>
                    </ul>
                </li>
            <?php endforeach ?>
        </ul>
    </div>
</div>