<div class="page-header">
    <h1>Задание 5 </h1>
</div>
<p class="lead">Выбрать из ране приведенной таблице все узлы которые имеют только двух старших родителей, но при этом не имеют потомков. (реализуется через SQL запрос)</p>
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">SQL Запрос</h3>
    </div>
    <div class="panel-body">
        <pre>SELECT t1.* FROM test.pages as t1
RIGHT JOIN pages as t2 ON t1.parent=t2.pid AND t2.parent!=0
RIGHT JOIN pages as t3 ON t2.parent=t3.pid AND t3.parent=0
WHERE t1.pid IS NOT NULL AND (SELECT COUNT(*) FROM test.pages as t4 WHERE t4.parent=t1.pid) = 0</pre>
    </div>
</div>
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Результат</h3>
    </div>
    <div class="panel-body tree-list">
        <pre><?php var_dump($rows);?></pre>
    </div>
</div>