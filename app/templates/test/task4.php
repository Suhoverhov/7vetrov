<div class="page-header">
    <h1>Задание 4 </h1>
</div>
<p class="lead">Выбрать из ране приведенной таблице все узлы которые не имеют родителей, но при этом содержать не менее 3 прямых потомков. (реализуется через SQL запрос)
</p>
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">SQL Запрос</h3>
    </div>
    <div class="panel-body">
        <pre>SELECT t1.* FROM test.pages as t1
WHERE t1.parent=0 AND (SELECT count(*) FROM test.pages as t2 WHERE t2.parent = t1.pid) >= 3</pre>
    </div>
</div>
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Результат</h3>
    </div>
    <div class="panel-body tree-list">
        <pre><?php var_dump($rows);?></pre>
    </div>
</div>