<div class="page-header">
    <h1>Задание 1 </h1>
</div>
<p class="lead">Есть массив BB кодов в текстовом представлении
    Преобразовать его в 2 PHP массива, где ключ массива – название BB кода, значение в одном массиве это данные BB кода, а в другом это описание BB кода
    * Ограничение – вложенность BB кодов не допускается, т.е не надо это учитывать в обработке. Коды “размазаны” по тексту и не следуют друг за другом.
    Описание BB кода начинается после символа “:”. Описание может отсутствовать. Закрывающий BB код – обязателен.
    Формат BB код: [ BB :ОПИСАНИЕ]ДАННЫЕ[/ BB ]</p>
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Входные даные</h3>
    </div>
    <div class="panel-body">
        <p class="lead"><?php echo $input_data;?></p>
        <p class="lead">Текст хранится в файле - <pre><?php echo$filePath;?></pre></p>
    </div>
</div>
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Результат</h3>
    </div>
    <div class="panel-body">
        <p class="lead">Массив данных BB кодов</p>
        <ul>
            <?php foreach($dataBb as $key=>$data):?>
                <li><code><?php echo $key . ' --> ' . $data?></code></li>
            <?php endforeach ?>
        </ul>
        <p class="lead">Массив описаний BB кодов</p>
        <ul>
            <?php foreach($descBb as $key=>$data):?>
                <li><code><?php echo $key . ' --> ' . $data?></code></li>
            <?php endforeach ?>
        </ul>
    </div>
</div>