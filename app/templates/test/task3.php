<div class="page-header">
    <h1>Задание 3 </h1>
</div>
<p class="lead">Есть таблица в которой храниться дерево:
    <pre>
    CREATE TABLE pages (
    pid int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    title varchar(255) NOT NULL,
    name varchar(255) default NULL,
    parent int(10) UNSIGNED default NULL,
    created int(10) NOT NULL,
    changed int(10) NOT NULL,
    PRIMARY KEY ( pid )) TYPE = MyISAM</pre>

    Описание необходимых полей(остальные опускаем) id - уникальный идентификатор узла дерева. Ограничения: Данное поле заполняется автоматически.
    name - Название узла дерева. Ограничения: Данное поле не обязательно к заполнению.
    parent - Идентификатор родительского узла(эта же таблица). Самый верхний узел имеет идентификатор NULL , т.е. тот который не имеет родителей. Ограничение: Данное поле не обязательно для заполнения и связано с полем id в этой же таблице.
    Необходимо написать код который на основе это таблицы построет дерево страниц
</p>
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Результат</h3>
    </div>
    <div class="panel-body tree-list">
        <?php echo $tree;?>
    </div>
</div>