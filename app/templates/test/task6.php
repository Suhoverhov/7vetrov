<div class="page-header">
    <h1>Задание 6 </h1>
</div>
<p class="lead">Найти повторяющееся числа в массиве с числами в диапазоне от 100 000 до 1 500 000. Количество повторяющихся чисел - 1-но, элементов в массиве - не меньше 1 000 000. Решить задачу на PHP минимальным использованием процессорного времени.</p>
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Входные данные</h3>
    </div>
    <div class="panel-body">
        <p class="lead">Числа хранятся в файле - <pre><?php echo$filePath;?></pre></p>
    </div>
</div>
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Результат</h3>
    </div>
    <div class="panel-body tree-list">
        <pre>Всего чисел: <?php echo $ctNumbers;?></pre>
        <pre>Количество повторяющихся чисел: <?php echo$ctRepeated;?></pre>
        <pre>Время поиска: <?php echo$time;?></pre>

<!--            Повторяющиеся числа: --><?php //echo$repeated;?>

            </pre>
    </div>
</div>