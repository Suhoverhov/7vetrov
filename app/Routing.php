<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 14.09.15
 * Time: 11:26
 */

namespace app {
    class Routing {

        var $main_action = 'task1'; // Функция, вызываемая по стандарту
        var $main_controller = 'Test'; // Контроллер, вызываемый по стандарту
        var $action_postfix = 'Action'; // Постфикс к действиям

        function __construct ()
        {
            $this->routs = explode('/', $_SERVER['REQUEST_URI']); // Разделяем наш запрос
                $this->controller = empty($this->routs[1]) ? $this->main_controller :ucfirst(strtolower($this->routs[1]));
                $this->action = empty($this->routs[2]) ? $this->main_action: $this->routs[2];
                $this->get_routs ();
        }

        function get_routs ()
        {
            $action = $this->action . $this->action_postfix;
            $controller = 'app\\controllers\\' . $this->controller;
            if(method_exists($controller, $action)){
                $obj = new $controller();
                $obj->$action();
            }
            else die('Возникла ошибка, ваш запрос не верен!');
        }
    }
}