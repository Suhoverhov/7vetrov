<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 16.09.15
 * Time: 1:04
 */

namespace app\controllers {
    use app\Controller;
    use app\models\Page;
    use app\models\PageModel;

    class Test extends Controller
    {
        private $rootDirFiles = '/www/text_data/';

        // задание 1
        public function task1Action() {
            $filePath = __ROOT__ . $this->rootDirFiles . 'bb_code.txt';
            $bb = file($filePath);
            $text = implode('', $bb);
            $re = "/\\[(?P<Bb>[^\\/\\[\\]\\:]*)(:(?P<desc>[^\\/\\[\\]]*))?\\](?P<data>[^\\[\\]]*)\\[\\/[^\\/\\[\\]]*\\]/";
            preg_match_all($re, $text, $output_array);

            $descBb = array();
            $dataBb = array();

            foreach($output_array['Bb'] as $key=>$item){
                $descBb[$item] = $output_array['desc'][$key];
                $dataBb[$item] = $output_array['data'][$key];
            }

            $this->render('task1', array('dataBb' => $dataBb, 'descBb' => $descBb, 'input_data' => $text, 'filePath' => $filePath));
        }

        // задание 2
        public function task2Action() {
            $filePath = __ROOT__ . $this->rootDirFiles . 'task2.txt';
            $data = file_get_contents($filePath);
            $re = "/(?P<name>post|get|url)\\:(?P<data>(?:.(?!(et\\:)|(ost\\:)|(rl\\:)))+)/s";
            preg_match_all($re, $data, $matches);
            $dataArray = array();
            foreach($matches['name'] as $key=>$item) {
                $dataArray[$item][] = $matches['data'][$key];
            }

            $this->render('task2', array('data' => $dataArray, 'input_data' => $data, 'filePath' => $filePath));
        }

        //задание 3
        public function task3Action() {
            $model = new PageModel();
            $pages = $model->getAllPages();
            $tree = $model->build_tree($pages,0);
            $this->render('task3', array('tree' => $tree));
        }

        //задание 4
        public function task4Action() {
            $model = new PageModel();
            $rows = $model->task4();

            $this->render('task4', array('rows' => $rows));
        }

        //задание 5
        public function task5Action() {
            $model = new PageModel();
            $rows = $model->task5();

            $this->render('task5', array('rows' => $rows));
        }

        //задание 6
        public function task6Action() {
            $filePath = __ROOT__ . $this->rootDirFiles . 'numbers.txt';
            $string = file_get_contents($filePath);
            $numbers = explode(' ', $string);

            $t1 = microtime(true);
            sort($numbers);
            $repeated = array();
            $size= count($numbers);
            for ($i = 0; $i < $size; $i++) {
                if(!isset($numbers[$i+1])) {
                    break;
                }
                if ($numbers[$i] == $numbers[$i+1]) {
                    $repeated[] = $numbers[$i];
                    $i++;
                }
            }
            $t2 = microtime(true);
            $diff = $t2 - $t1;
            $this->render('task6',array('ctNumbers' => $size, 'ctRepeated' => count($repeated),
                'repeated' => implode(', ',$repeated), 'time' => $diff, 'filePath' => $filePath));
        }

        /**
         * Метод генерации чисел, генерит и записывает их в файл
         */
        private function generateNumbers() {
            $array_numbers = range(100000, 1200000);
            $arrayGen = array();
            for($i = 0; $i < 200000; $i++) {
                $num = rand(1000000, 1500000);
                if(!in_array($num, $arrayGen)) {
                    $arrayGen[] = $num;
                }
            }
            $newArray = array_merge($array_numbers, $arrayGen);

            shuffle($newArray);
            file_put_contents(__ROOT__ . $this->rootDirFiles . 'numbers.txt', implode(' ',$newArray));
        }

        //задание 7
        public function task7Action() {

            $this->render('task7');
        }
    }
}