<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 16.09.15
 * Time: 1:04
 */

namespace app {

    class Controller
    {
        protected static $rootDirTemplates = '/app/templates/';

        protected function render($view, $data = array()){
            extract($data);
            $pathToView = $this->getPathToView() . '/' . $view . '.php';
            ob_start();
            require $pathToView;
            $content = ob_get_clean();
            $mainTemplate = __ROOT__ . '/app/templates/main.php';
            require($mainTemplate);
        }

        protected function getPathToView(){
            $function = new \ReflectionClass($this);
            return __ROOT__ . self::$rootDirTemplates . strtolower($function->getShortName());
        }
    }
}