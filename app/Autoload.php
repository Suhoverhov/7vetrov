<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 14.09.15
 * Time: 11:26
 */

final class Autoload
{
    private static $dirs = array(
        '/app/controllers',
        '/app/models',
    ); // массив директорий с классами, которые нужно загрузить

    private static $includes = array(); // массив директорий с классами, которые нужно загрузить

    public static function run(){
        require_once(__ROOT__ . '/app/Application.php');
        require_once(__ROOT__ . '/app/Routing.php');
        require_once(__ROOT__ . '/app/Controller.php');
        require_once(__ROOT__ . '/app/Model.php');
        self::getClassDir();
    }

    public static function getClassDir(){
        $files = array();
        foreach( self::$dirs as $dir){
            $pathToDir = __ROOT__ . $dir;
            $scan = scandir($pathToDir, 1);
            foreach ($scan as $file) {
                if ($file != '.' && $file != '..') {
                    $files[] = $pathToDir . '/' . $file;
                }
            }
        }

        foreach($files as $item){
            require_once($item);
        }
    }
}
spl_autoload_register(array('Autoload', 'run'), true, true);
