<?php
/**
 * Created by PhpStorm.
 * User: denis
 * Date: 16.09.15
 * Time: 1:04
 */

namespace app\models {
    use app\Model;

    class PageModel extends Model
    {
        public function getAllPages() {
            //Выбираем данные из БД
            $result = mysql_query("SELECT * FROM  test.pages");
            //Если в базе данных есть записи, формируем массив
            if   (mysql_num_rows($result) > 0){
                $cats = array();
                //В цикле формируем массив разделов, ключом будет id родительской категории, а также массив разделов, ключом будет id категории
                while($cat = mysql_fetch_assoc($result)){
                    $cats[$cat['parent']][$cat['pid']] =  $cat;
                }
                return $cats;
            }
            return null;
        }

        public function build_tree($cats,$parent_id, $deep=0){
            if(is_array($cats) and isset($cats[$parent_id])){
                $arrow = $parent_id != 0 ? str_repeat('-',$deep) .'>' : '';
                $tree = '<ul>';
                foreach($cats[$parent_id] as $cat){
                    $tree .= '<li>'.$arrow.$cat['name'];
                    $tree .=  $this->build_tree($cats,$cat['pid'], $deep + 1);
                    $tree .= '</li>';
                }
                $tree .= '</ul>';
            }
            else return null;
            return $tree;
        }

        /**
         * Выбрать из ране приведенной таблице все узлы которые не имеют родителей, но при этом содержать не менее 3 прямых потомков. (реализуется через SQL запрос)
         * @return array
         */
        public function task4(){
            $result = mysql_query("
                SELECT t1.* FROM test.pages as t1
                WHERE t1.parent=0
                AND (
                    SELECT count(*)
                    FROM test.pages as t2
                    WHERE t2.parent = t1.pid
                ) >= 3"
            );
            $data = array();
            while($row = mysql_fetch_assoc($result))
            {
                $data[] = $row;
            }
            return $data;
        }

        /**
         * Выбрать из ране приведенной таблице все узлы которые имеют только двух старших родителей, но при этом не имеют потомков. (реализуется через SQL запрос)
         * @return array
         */
        public function task5(){
            $result = mysql_query("
                SELECT t1.* FROM test.pages as t1
                RIGHT JOIN test.pages as t2 ON t1.parent=t2.pid AND t2.parent!=0
                RIGHT JOIN test.pages as t3 ON t2.parent=t3.pid AND t3.parent=0
                WHERE t1.pid IS NOT NULL
                AND (SELECT COUNT(*) FROM test.pages as t4 WHERE t4.parent=t1.pid) = 0"
            );
            $data = array();
            while($row = mysql_fetch_assoc($result))
            {
                $data[] = $row;
            }
            return $data;
        }
    }
}